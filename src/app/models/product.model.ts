export interface Product {
    product_name_fr: string;
    image_url: string;
    brands: string;
    ingredients_text_fr: string;
    pnns_groups_1_tags: string[];
    ingredients_analysis_tags: string[];
    // TODO: à compléter avec ce qu'on a besoin
}
